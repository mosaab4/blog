# Django Blogs
django blogs is a simple blog using [django 2.0 ](https://docs.djangoproject.com/en/2.0/releases/2.0/), the blog contains basic tasks like adding and removing posts.
also have an authentication system for registering users.
<hr>

## Features

1. adding new posts
2. deleting posts
3. like a post or remove the like
4. viwe list of posts or specific post
5. upload images for your blogs
6. searching posts by  name, text, author and category
7. admin page where you can do Create, Read, Update and delete posts
8. full featured pagedown content creation
9. authentication system where you can perform :
    1. Registeration
    2. Login/Logout
    3. Change password
    4. Forgt/Reset Password
<hr>

## Requirments

* [django 2.0 ](https://docs.djangoproject.com/en/2.0/releases/2.0/)
*  [virtualenv](http://www.virtualenv.org/en/latest/)
*  [Pillow](https://pillow.readthedocs.io/en/5.1.x/)
*  [django-markdown-deux](https://github.com/trentm/django-markdown-deux)
*  [django-pagedown](https://github.com/timmyomahony/django-pagedown)
*  [djangorestframework](http://www.django-rest-framework.org/)
*  [markdown2](https://github.com/trentm/python-markdown2)
<hr>

## What's included

```
blog\
    ├── accounts\
    |   ├── tests\
    |   |   ├── test_password_reset_view.py
    |   |   ├── test_sign_up_form.py
    |   |   ├── test_sign_up_view.py
    |   |   ├── test_suuccessfull_signup.py
    |   |   └── test_successfull_password_reset.py
    |   ├── admin.py
    |   ├── apps.py
    |   ├── models.py
    |   ├── urls.py
    |   └── views.py
    ├── blog\
    |   ├── settings.py
    |   ├── urls.py
    |   └── wsgi.py
    ├── posts
    |   ├── tests\
    |   |   ├── test_create_post_form.py
    |   |   ├── test_post_detail.py
    |   |   └── test_view_home.py
    |   ├── admin.py
    |   ├── apps.py
    |   ├── forms.py
    |   ├── models.py
    |   ├── serializers.py
    |   ├── urls.py
    |   └──  views.py
    └── templates\
        ├── base.html
        ├── change_password.html
        ├── login.html
        ├── logout.html
        ├── navbar.html
        ├── password_reset_complete.html
        ├── password_reset_confirm.html
        ├── password_reset_done.html
        ├── password_reset_email.html
        ├── password_reset_form.html
        ├── password_reset_subject.txt
        ├── post_detail.html
        ├── post_form.html
        ├── post_list.html
        └── signup.html
```
<hr>

## How to start
1. first clone the repo
``` 
    $ git clone <url>
```

2. cd to blog directory 
```
    $ cd blog/blog
```
 3. create a superuser
``` 
    $ python manage.py createsuperuser 
```
4. run the server
``` 
    $ python manage.py runserver 
```

<hr>

## Urls mapping
url mapping for posts app
``` 
    /posts/                           home page
    /posts/create/                    create new post
    /posts/<int:id>/                  specific post
    /posts/<int:id>/delete/           delete specific post
    /posts/<int:id>/like/             like/dislike the post
    /posts/api/                       view API for the post list
    /posts/api/<int:pk>/              view API for specific post + destroying or editing the post
```

url mapping for accounts app
```
    /accounts/login/                    login the user
    /accounts/logout/                   logout the user
    /accounts/signup/                   signup for new users
    /accounts/password/                 change password for users
    /accounts/password_reset/           reset password
    /accounts/password_reset/done/      reseting password done
    /accounts/reset/<uidb64>/<token>/   reset password
    /accounts/reset/done/               reset password complete

```

<hr>

## What's next?
- [ ] complete testing
- [ ] API for Accounts
- [ ] Slug Field
- [ ] Pagination
- [ ] adding django crispy forms
- [ ] add delete button to detail view
- [ ] reorganizing templates
- [ ] fully implementing bootstrap

<hr>

## Trello
here's [trello's board](https://trello.com/b/vu3u0GAc/intern-task) for the blog.

<hr>

## License
the code and documentaion is under [Creative Commons](https://creativecommons.org/) license