from django.test import TestCase
from django.urls import reverse,resolve
from django.contrib.auth.models import User

from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm

from ..views import SignUp

class SignUpTests(TestCase):
    def setUp(self):
        url = reverse('accounts:signup')
        self.response = self.client.get(url)

    def test_signup_status_code(self):
        self.assertEquals(self.response.status_code, 200)

    def test_signup_url_resolves_signup_view(self):
        view = resolve('/accounts/signup/')
        self.assertEquals(view.func.view_class, SignUp)

    def test_contains_form(self):
        form = self.response.context.get('form')
        self.assertIsInstance(form ,UserCreationForm)
        
    def test_csrf(self):
        self.assertContains(self.response, 'csrfmiddlewaretoken')

    def test_form_inputs(self):
        self.assertContains(self.response, '<input', 6)
        self.assertContains(self.response, 'type="text"', 2)
        self.assertContains(self.response, 'type="password"',2)
