from django.test import TestCase
from django.urls import reverse,resolve
from django.contrib.auth.models import User

from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm

from ..views import SignUp


# Create your tests here.

class SuccessfullSignUpTests(TestCase):
    def setUp(self):
        url = reverse('accounts:signup')
        data = {
            'username': 'mosaab',
            'password1':'abcd123',
            'password2':'abcd123',
        }
        self.response = self.client.post(url, data)
        self.home_url = reverse('posts:list')

    def test_redirection(self):
        pass
        # self.assertRedirects(self.response,self.home_url)

    def test_user_creation(self):
        pass
        # self.assertTrue(User.objects.exists())

    def test_user_authentication(self):
        response = self.client.get(self.home_url)
        user = response.context.get('user')
        # self.assertTrue(user.is_authenticated)