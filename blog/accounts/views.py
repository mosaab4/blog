from django.shortcuts import render, redirect
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django.urls import reverse_lazy
from django.views import generic



# Create your views here.


class SignUp(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('accounts:login')
    template_name = 'signup.html'


def change_password(request):
    if request.method == "POST":
        form = PasswordChangeForm(request.user, request.POST)

        if form.is_valid():
            user = form.save()
            #update hash for the current session
            update_session_auth_hash(request,user)
            return redirect("accounts:change_password")
    else :
        form = PasswordChangeForm(request.user)

    context= {
        "form":form,
    }

    return render(request,"change_password.html", context)
