from django.urls import path
from django.conf.urls import url
from django .contrib import admin
from django.contrib.auth import views as auth_views

from .views import SignUp,change_password

app_name = 'accounts'
urlpatterns = [
    path('login/', auth_views.login,{"template_name":"login.html"}, name="login"),
    path('logout/', auth_views.logout,{"template_name":"logout.html"}, name="logout"),
    path('signup/', SignUp.as_view(), name='signup'),
    path('password/', change_password, name='change_password'),
    
    
    path(
        'password_reset/', 
        auth_views.password_reset,
        {
            "template_name":"password_reset_form.html",
            "subject_template_name":"password_reset_subject.txt"
            
        }, 
        name='password_reset'),
    
    path('password_reset/done', auth_views.password_reset_done,{"template_name":"password_reset_done.html"}, name="password_reset_done"),
    
    path('reset/<uidb64>/<token>/', auth_views.password_reset_confirm,{'template_name':'password_reset_confirm.html'},name="password_reset_confirm"),
    path('reset/done/', auth_views.password_reset_complete, {'template_name':'password_reset_complete.html'},name="password_reset_complete")
    
]

