from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from django.conf import settings

from markdown_deux import markdown


def upload_location(instance,filename):
    return 'user_%s/%s'%(instance.id, filename)

    
# Create your models here.
class Category(models.Model):
    title = models.CharField(max_length=255)


    def __str__(self):
        return self.title

    class Meta: 
        verbose_name_plural = "Categories"


class Post(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=150)
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False , auto_now=True)
    
    image = models.ImageField(upload_to = upload_location, blank=True, null=True)
    category = models.ForeignKey(Category,on_delete=models.CASCADE,default="")

    #likes here related to user by many to many relationship
    likes = models.ManyToManyField(User, blank=True ,related_name="post_likes")
   
    def __str__(self):
        return self.title


    def get_absolute_url(self):
        #return the absolute url of the post
        return reverse("posts:detail", kwargs={"id":self.id})

    def get_like_url(self):
        # retutn page post likes
        return reverse("posts:like-toggle", kwargs={"id":self.id})

    def get_markdown(self):
        content = self.content
        #return marked down content
        return markdown(content)


