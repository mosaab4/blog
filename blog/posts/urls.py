from django.urls import path
from .views import (
    post_detail,
    post_list,
    post_delete,
    post_create,
    PostLikeToggle,
    PostList,
    PostDetail
)

app_name = "posts"
urlpatterns = [
    path('', post_list, name="list"),
    path('<int:id>/', post_detail, name="detail"),
    path('<int:id>/like', PostLikeToggle.as_view(), name="like-toggle"),
    path('<int:id>/delete/', post_delete, name="delete"),
    path('create/', post_create, name="create"),
    path('api/',PostList.as_view()),
    path('api/<int:pk>', PostDetail.as_view())
]