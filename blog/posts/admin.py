from django.contrib import admin
from pagedown.widgets import AdminPagedownWidget
from django.db import models
# Register your models here.

from .models import Post,Category

class PostAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "timestamp",
        "updated",
    )

    search_fields = ('title','content')
    #filtering posts by timestamp,updated and category
    list_filter = (
        "timestamp",
        "updated",
        "category"
        )
    # add pagedown to admin page
    formfield_overrides = {
        models.TextField:{
            'widget':AdminPagedownWidget
        }
    }

class CategoriesAdmin(admin.ModelAdmin):
    search_fields = ("title",)
    
admin.site.register(Post, PostAdmin)
admin.site.register(Category, CategoriesAdmin)