from django import forms
from django.forms import ModelForm
from pagedown.widgets import PagedownWidget

from .models import Post


class PostForm(ModelForm):
    # add pagedown to content text field
    content = forms.CharField(widget=PagedownWidget())
    class Meta:
        model = Post 
        fields = [
            "title",
            "content",
            "image",
            "category",
        
        ]
