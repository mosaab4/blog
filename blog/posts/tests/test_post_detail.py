from django.test import TestCase
from django.urls import reverse,resolve
from django.contrib.auth.models import User

from ..views import post_list, post_detail,post_create
from ..models import Post, Category
from ..forms import PostForm

# Create your tests here.


class TestPostDetail(TestCase):
    def setUp(self):
        test_user = User.objects.create_superuser(username="mosaab",email="sss@dsk.sos", password="123456789")
        test_user.save()

        category = Category.objects.create(title="art")
        category.save()

        Post.objects.create(user= test_user,title="lorem ipsum", content="lorem ipsum dolor sit", category=category)
    
    def post_list_view_success_status_code(self):
        url = reverse("posts:detail", kwargs={"pk":1})
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def post_list_view_not_found(self):
        url = reverse("posts:detail", kwargs={'pk':99})
        response = self.client.get(url)
        self.assertEquals(response.status_code, 404)

    def post_list(self):
        view = resolve('/posts/detail/1/')
        self.assertEquals(view.func, post_detail)

