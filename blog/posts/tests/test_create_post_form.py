from django.test import TestCase
from django.urls import reverse,resolve
from django.contrib.auth.models import User

from ..views import post_list, post_detail,post_create
from ..models import Post, Category
from ..forms import PostForm

# Create your tests here.


class CreatePostTest(TestCase):
    def setUp(self):
        test_user = User.objects.create_superuser(username="mosaab",email="sss@dsk.sos", password="123456789")
        test_user.save()

        category = Category.objects.create(title="art")
        category.save()

        # Post.objects.create(user= test_user,title="lorem ipsum", content="lorem ipsum dolor sit", category=category)
    
    def test_create_post_view_success_status_code(self):
        url = reverse('posts:create')
        response = self.client.get(url)
        self.assertEquals(response.status_code,200)

    def test_create_post_url_resolves_post_create_view(self):
        view = resolve('/posts/create/')
        self.assertEquals(view.func, post_create)

    def test_create_post_view_contains_url_back_to_home(self):
        new_post_url = reverse('posts:create')
        home_url = reverse('posts:list')

        response = self.client.get(new_post_url)

        self.assertContains(response, 'href="{0}"'.format(home_url))

    def test_csrf(self):
        url = reverse("posts:create")
        response = self.client.get(url)
        self.assertContains(response, 'csrfmiddlewaretoken')

    def test_create_post_valid_post_date(self):
        url = reverse('posts:create')
        data = {
            'title':'lorem ipsum',
            'content':'lorem ipsum dolor sit',
            'category':'art',
        }

        response = self.client.post(url, data)
        # self.assertTrue(Post.objects.exists())

    def test_create_post_invalid_data(self):
        url = reverse("posts:create")
        response = self.client.post(url, {})
        form = response.context.get('form')

        self.assertEquals(response.status_code , 200)
        # self.assertTrue(form.errors)

    def test_create_post_invali_post_data_empty_fields(self):
        url = reverse('posts:create')
        data = {
            'title':'',
            'content':'',
            'category':'',
        }
        response = self.client.post(url, data)
        
        self.assertEquals(response.status_code, 200)

        self.assertFalse(Post.objects.exists())

    def test_contains_form(self):
        url = reverse("posts:create")
        response = self.client.get(url)
        form = response.context.get('form')

        self.assertIsInstance(form, PostForm)
