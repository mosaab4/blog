from django.test import TestCase
from django.urls import reverse,resolve
from django.contrib.auth.models import User

from ..views import post_list, post_detail,post_create
from ..models import Post, Category
from ..forms import PostForm

# Create your tests here.


class TestHome(TestCase):
    def setUp(self):
        test_user = User.objects.create_superuser(username="mosaab",email="sss@dsk.sos", password="123456789")
        test_user.save()

        category = Category.objects.create(title="art")
        category.save()

        self.post = Post.objects.create(user= test_user,title="lorem ipsum", content="lorem ipsum dolor sit", category=category)
        url = reverse('posts:list')
        self.response = self.client.get(url)

    def test_home_status_code(self):
        self.assertEquals(self.response.status_code , 200)

    def test_home_resolves_home_view(self):
        view = resolve('/posts/')
        self.assertEquals(view.func, post_list)

    def test_home_view_contains_links_to_details_page(self):
        post_detail_url = reverse('posts:detail',kwargs={"id": self.post.id})
        self.assertContains(self.response , 'href="{0}"'.format(post_detail_url))
