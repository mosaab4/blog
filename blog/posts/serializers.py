from rest_framework import serializers
from .models import Post

class PostSerializer(serializers.ModelSerializer):
    # serialize the following fileds
    class Meta:
        fields = (
            'id',
            'user',
            'title',
            'content',
            'timestamp',
            'image',
            'category',
            'likes'
        )
        model =Post