from django.shortcuts import render,get_object_or_404, redirect
from django.http import (
    HttpResponse, 
    HttpResponseRedirect,
    Http404
)
from django.db.models import Q

from django.views.generic import RedirectView

from rest_framework.generics import (
    ListAPIView,
    RetrieveUpdateDestroyAPIView
)
from .models import Post
from .forms import PostForm
from .serializers import PostSerializer

# Create your views here.

def post_list(request):
    posts = Post.objects.all()

    #searching the post by title,content,username and category
    query = request.GET.get('q')

    if query :
        posts = posts.filter(
            Q(title__icontains=query)|
            Q(content__icontains=query)|
            Q(user__username__icontains=query)|
            Q(category__title__icontains=query)
            
        ).distinct()
    context = {
        "posts": posts,
    }

    return render(request, "post_list.html", context)

def post_detail(request, id=None):
    post = get_object_or_404(Post, id=id)
    
    context = {
        "post": post,
    }

    return render(request, "post_detail.html", context)

def post_create(request):
    
    if request.method == "POST":
        form = PostForm(request.POST or None, request.FILES or None)

        if form.is_valid():
            post = form.save(commit=False)
            post.user = request.user
            post.save()

            return HttpResponseRedirect(post.get_absolute_url())
    else :
        form = PostForm()
    context = {
        "form":form,
    }

    return render(request,"post_form.html", context)

def post_delete(request, id=None):
    post = get_object_or_404(Post, id=id)
    
    #checking if user is superuser
    if not request.user.is_staff or not request.user.is_authenticated:
        raise Http404
    post.delete()

    return redirect("posts:list")

class PostLikeToggle(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        #get the id and search the post by id
        id = self.kwargs.get("id")
        post = get_object_or_404(Post, id=id)
        url_ = post.get_absolute_url()
        user = self.request.user
        #if user in likes then remove him if not add him
        if user.is_authenticated:
            if user in post.likes.all():
                post.likes.remove(user)
            else:
                post.likes.add(user)

        return url_


#code for api list, retriebe, update and destroy view
class PostList(ListAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

class PostDetail(RetrieveUpdateDestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
