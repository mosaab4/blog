Django==2.0.5
django-markdown-deux==1.0.5
django-pagedown==1.0.4
djangorestframework==3.8.2
markdown2==2.3.5
Pillow==5.1.0
pytz==2018.4
